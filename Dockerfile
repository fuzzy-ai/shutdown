FROM nginx:1-alpine

COPY web /usr/share/nginx/html

COPY default.conf /etc/nginx/conf.d/default.conf

RUN apk add --no-cache curl
